package it.unibo.oop.lab.enum1;

import it.unibo.oop.lab.socialnetwork.User;

/**
 * This is going to act as a test for
 * {@link it.unibo.oop.lab.enum1.SportSocialNetworkUserImpl}
 * 
 * 1) Realize the same test as the previous exercise, this time using
 * enumeration Sport
 * 
 * 2) Run it: every test must return true.
 * 
 */
public final class TestSportByEnumeration {

    private TestSportByEnumeration() {
    }

    /**
     * @param args
     *            ignored
     */
    public static void main(final String... args) {
        /*
         * [TEST DEFINITION]
         * 
         * By now, you know how to do it
         */
        // TODO
    	
    	final SportSocialNetworkUserImpl<User> cicciocaputo = new SportSocialNetworkUserImpl<User>("Ciccio", "Caputo", "cicciocaputo", 30);
    	final SportSocialNetworkUserImpl<User> laxalt = new SportSocialNetworkUserImpl<User>("Boh", "Laxalt", "laxalt", 24);
    	final SportSocialNetworkUserImpl<User> dybi = new SportSocialNetworkUserImpl<User>("paulo", "dybala", "dybi", 26);
    	
    	cicciocaputo.addSport(Sport.SOCCER);
    	laxalt.addSport(Sport.BASKET);
    	dybi.addSport(Sport.F1);
    	
    	System.out.println("likes: " + cicciocaputo.hasSport(Sport.TENNIS));
    	System.out.println("likes: " + laxalt.hasSport(Sport.BASKET));
    }

}
